output "sonarqube_ec2_public_dns" {
    description = "sonarqube ec2 public dns value"
    value       = module.resource_group.sonarqube_ec2_public_dns
}
